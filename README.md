# Tests de regresión con BackstopJS

Backstop es una herramienta de tests de regresión visuales, es decir, nos avisa cuando al trabajar en un proyecto una página que se veía bien cambió inadvertidamente.

La primera vez se hace que la herramienta saque screenshot de nuestras páginas y tome esas fotos como referencia. 
Luego mientras vamos desarrollando, podemos correr los tests de regresión para que saque screenshots del estado actual y los compare con las referencias. Las diferencias se verán en un reporte HTML. Si revisamos las diferencias y son intencionales, podemos aprobar esos escenarios para tomarlos como nueva referencia.

## Cómo agregar Backstop a un proyecto con npm:
1. `npm install backstopjs` descargar y agregar backstopjs
2. `npx backstop init` para crear la configuración `backstop.json` y las carpetas donde pone los resultados de tests (`backstop_data`)

## Cómo utilizarlo
1. Correr los tests con `npm test`. Las páginas que correrá y otras configuraciones están en `backstop.json`. Se puede elegir sólo los tests que contengan una palabra haciendo `npm test -- --filter=palabra`
2. Revisar las diferencias indicadas. Si alguna es intencional, aprobar con `npm run approve -- --filter=nombreescenario`. Si todas están bien, aprobar todo con `npm run approve`
